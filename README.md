# Building A Face Detection App with Streamlit and OpenCV

## Deployment
Deploy to Heroku by gitlab CI

## Requirements
* numpy==1.19.5
* opencv-python==4.5.1.48
* pandas==1.1.5
* Pillow==8.1.0
* requests==2.25.1
* streamlit==0.76.0
* opencv-contrib-python-headless
