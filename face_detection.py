import cv2
import numpy as np
import streamlit as st
from PIL import Image, ImageEnhance
from random import randrange
import json

trained_face_data = cv2.CascadeClassifier(
    'models/haarcascade_frontalface_default.xml')
trained_smile_data = cv2.CascadeClassifier('models/haarcascade_smile.xml')


def detect_face(img):
    new_img = np.array(img.convert('RGB'))
    img = cv2.cvtColor(new_img, 1)

    # Must convert to grayscale
    grayscaled_img = cv2.cvtColor(new_img, cv2.COLOR_BGR2GRAY)

    # Detect face
    face_coordinates = trained_face_data.detectMultiScale(grayscaled_img)

    # Draw rectangle around the face
    for (x, y, w, h) in face_coordinates:
        cv2.rectangle(img, (x, y), (x + w, y + h), (randrange(150,
                                                              256), randrange(150, 256), randrange(150, 256)), 2)

    # cv2.imshow('Clever Programmer Face Detector', new_img)
    # cv2.waitKey()
    return img, face_coordinates


def detect_smile(img):
    new_img = np.array(img.convert('RGB'))
    img = cv2.cvtColor(new_img, 1)
    gray = cv2.cvtColor(new_img, cv2.COLOR_BGR2GRAY)
    # Detect Smiles
    smiles = trained_smile_data.detectMultiScale(gray, 1.1, 4)
    # Draw rectangle around the Smiles
    for (x, y, w, h) in smiles:
        cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)
    return img


def cannize_image(img):
    new_img = np.array(img.convert('RGB'))
    img = cv2.cvtColor(new_img, 1)
    img = cv2.GaussianBlur(img, (11, 11), 0)
    canny = cv2.Canny(img, 100, 150)
    return canny


def main():
    """ Main App  """
    st.set_page_config(layout="wide")

    st.title("Face Detection")

    expand_bar = st.beta_expander("About")
    expand_bar.markdown("""
    * **Data Source:** [Opencv Data](https://github.com/opencv/opencv/tree/master/data/haarcascades)
    """)
    # Choose an image to detect faces in
    img = st.file_uploader("Upload Image", type=['jpg', 'png', 'jpeg'])

    col1, col2 = st.beta_columns((2, 1))
    if img is not None:
        img = Image.open(img)
        task = ["Faces", "Cannize"]
        feature_choice = st.sidebar.selectbox("Find Features",task)
        if feature_choice == "Faces":
            transform_img, result_faces = detect_face(img)
            col1.success("**{}** faces where found. Hurray!".format(len(result_faces)))
            col1.image(transform_img)
        elif feature_choice == "Cannize":
            transform_img = cannize_image(img)
            col1.success("Transform Successfully")
            col1.image(transform_img)
        
        col2.write("Original Image")
        col2.image(img)
        


if __name__ == '__main__':
    main()
